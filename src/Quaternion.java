import java.util.*;

/**
 * Quaternions. Basic operations.
 */
public class Quaternion {

    private final double a;
    private final double b;
    private final double c;
    private final double d;

    /**
     * Constructor from four double values.
     *
     * @param a real part
     * @param b imaginary part i
     * @param c imaginary part j
     * @param d imaginary part k
     */
    public Quaternion(double a, double b, double c, double d) {
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    /**
     * Real part of the quaternion.
     *
     * @return real part
     */
    public double getRpart() {
        return a;
    }

    /**
     * Imaginary part i of the quaternion.
     *
     * @return imaginary part i
     */
    public double getIpart() {
        return b;
    }

    /**
     * Imaginary part j of the quaternion.
     *
     * @return imaginary part j
     */
    public double getJpart() {
        return c;
    }

    /**
     * Imaginary part k of the quaternion.
     *
     * @return imaginary part k
     */
    public double getKpart() {
        return d;
    }

    /**
     * Conversion of the quaternion to the string.
     *
     * @return a string form of this quaternion:
     * "a+bi+cj+dk"
     * (without any brackets)
     */
    @Override
    public String toString() {
        String bWithSign;
        String cWithSign;
        String dWithSign;
        if (b >= 0) {
            bWithSign = "+" + b + "i";
        }
        else if (b == 0) {
            bWithSign = "";
        }
        else {
            bWithSign = b + "i";
        }

        if (c > 0) {
            cWithSign = "+" + c + "j";
        }
        else if (c == 0) {
            cWithSign = "";
        }
        else {
            cWithSign = c + "j";
        }

        if (d >= 0) {
            dWithSign = "+" + d + "k";
        }
        else if (d == 0) {
            dWithSign = "";
        }
        else {
            dWithSign = d + "k";
        }

        return String.format("%s%s%s%s", a, bWithSign, cWithSign, dWithSign);

    }

    /**
     * Conversion from the string to the quaternion.
     * Reverse to <code>toString</code> method.
     *
     * @param s string of form produced by the <code>toString</code> method
     * @return a quaternion represented by string s
     * @throws IllegalArgumentException if string s does not represent
     *                                  a quaternion (defined by the <code>toString</code> method)
     */
    public static Quaternion valueOf(String s) {
        List<Character> imaginaryParts = Arrays.asList('i', 'j', 'k');
        StringBuilder number = new StringBuilder();
        List<Double> parts = new ArrayList<>();

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '+') {
                parts.add(Double.parseDouble(number.toString()));
                number = new StringBuilder();
            } else if (s.charAt(i) == '-') {
                if (number.length() != 0) {
                    parts.add(Double.parseDouble(number.toString()));
                    number = new StringBuilder();
                }
                number.append(s.charAt(i));
            } else if (s.charAt(i) == 'j') {
                if (parts.size() < 2) {
                    parts.add(0.0);
                }
            } else if (s.charAt(i) == 'k') {
                if (parts.size() < 2) {
                    parts.add(0.0);
                    parts.add(0.0);
                }
                if (parts.size() < 3) {
                    parts.add(0.0);
                }
            } else if (!imaginaryParts.contains(s.charAt(i))) {
                number.append(s.charAt(i));
            }
        }
        parts.add(Double.parseDouble(number.toString()));
        while (parts.size() < 4) {
            parts.add(0.0);
        }
        return new Quaternion(parts.get(0), parts.get(1), parts.get(2), parts.get(3));
    }

    /**
     * Clone of the quaternion.
     *
     * @return independent clone of <code>this</code>
     */
    @Override
    public Object clone() throws CloneNotSupportedException {
        return new Quaternion(a, b, c, d);
    }

    /**
     * Test whether the quaternion is zero.
     *
     * @return true, if the real part and all the imaginary parts are (close to) zero
     */
    public boolean isZero() {
        double epsilon = 0.000001;
        return a - epsilon < 0 && a + epsilon > 0 &&
                b - epsilon < 0 && b + epsilon > 0 &&
                c - epsilon < 0 && c + epsilon > 0 &&
                d - epsilon < 0 && d + epsilon > 0;
    }

    /**
     * Conjugate of the quaternion. Expressed by the formula
     * conjugate(a+bi+cj+dk) = a-bi-cj-dk
     *
     * @return conjugate of <code>this</code>
     */
    public Quaternion conjugate() {
        return new Quaternion(a, b * -1, c * -1, d * -1);
    }

    /**
     * Opposite of the quaternion. Expressed by the formula
     * opposite(a+bi+cj+dk) = -a-bi-cj-dk
     *
     * @return quaternion <code>-this</code>
     */
    public Quaternion opposite() {
        return new Quaternion(a * -1, b * -1, c * -1, d * -1);
    }

    /**
     * Sum of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) + (a2+b2i+c2j+d2k) = (a1+a2) + (b1+b2)i + (c1+c2)j + (d1+d2)k
     *
     * @param q addend
     * @return quaternion <code>this+q</code>
     */
    public Quaternion plus(Quaternion q) {
        return new Quaternion(a + q.a, b + q.b, c + q.c, d + q.d);
    }

    /**
     * Product of quaternions. Expressed by the formula
     * (a1+b1i+c1j+d1k) * (a2+b2i+c2j+d2k) = (a1a2-b1b2-c1c2-d1d2) + (a1b2+b1a2+c1d2-d1c2)i +
     * (a1c2-b1d2+c1a2+d1b2)j + (a1d2+b1c2-c1b2+d1a2)k
     *
     * @param q factor
     * @return quaternion <code>this*q</code>
     */
    public Quaternion times(Quaternion q) {
        double newA = a * q.a - b * q.b - c * q.c - d * q.d;
        double newB = a * q.b + b * q.a + c * q.d - d * q.c;
        double newC = a * q.c - b * q.d + c * q.a + d * q.b;
        double newD = a * q.d + b * q.c - c * q.b + d * q.a;

        return new Quaternion(newA, newB, newC, newD);
    }

    /**
     * Multiplication by a coefficient.
     *
     * @param r coefficient
     * @return quaternion <code>this*r</code>
     */
    public Quaternion times(double r) {
        return new Quaternion(a * r, b * r, c * r, d * r);
    }

    /**
     * Inverse of the quaternion. Expressed by the formula
     * 1/(a+bi+cj+dk) = a/(a*a+b*b+c*c+d*d) +
     * ((-b)/(a*a+b*b+c*c+d*d))i + ((-c)/(a*a+b*b+c*c+d*d))j + ((-d)/(a*a+b*b+c*c+d*d))k
     *
     * @return quaternion <code>1/this</code>
     */
    public Quaternion inverse() {
        if (this.isZero()) {
            throw new RuntimeException(String.format("Cannot inverse quaternion %s. It is equal to 0.", this.toString()));
        }
        return new Quaternion(inverseHelper(a), inverseHelper(-b), inverseHelper(-c), inverseHelper(-d));
    }

    private double inverseHelper(double l) {
        return l / (a * a + b * b + c * c + d * d);
    }

    /**
     * Difference of quaternions. Expressed as addition to the opposite.
     *
     * @param q subtrahend
     * @return quaternion <code>this-q</code>
     */
    public Quaternion minus(Quaternion q) {
        return new Quaternion(a - q.a, b - q.b, c - q.c, d - q.d);
    }

    /**
     * Right quotient of quaternions. Expressed as multiplication to the inverse.
     *
     * @param q (right) divisor
     * @return quaternion <code>this*inverse(q)</code>
     */
    public Quaternion divideByRight(Quaternion q) {
        if (q.isZero()) {
            throw new RuntimeException(String.format("Cannot inverse quaternion %s. It is equal to 0.", q.toString()));
        }
        return this.times(q.inverse());
    }

    /**
     * Left quotient of quaternions.
     *
     * @param q (left) divisor
     * @return quaternion <code>inverse(q)*this</code>
     */
    public Quaternion divideByLeft(Quaternion q) {
        if (q.isZero()) {
            throw new RuntimeException(String.format("Cannot inverse quaternion %s. It is equal to 0.", q.toString()));
        }
        return q.inverse().times(this);
    }

    /**
     * Equality test of quaternions. Difference of equal numbers
     * is (close to) zero.
     *
     * @param qo second quaternion
     * @return logical value of the expression <code>this.equals(qo)</code>
     */
    @Override
    public boolean equals(Object qo) {
        if (!(qo instanceof Quaternion)) {
            return false;
        }
        Quaternion q = (Quaternion) qo;
        double epsilon = 0.000001;

//        return this.hashCode() == q.hashCode();

        return a - epsilon < q.a && a + epsilon > q.a &&
                b - epsilon < q.b && b + epsilon > q.b &&
                c - epsilon < q.c && c + epsilon > q.c &&
                d - epsilon < q.d && d + epsilon > q.d;
    }

    private Quaternion divide(double divider) {
        return new Quaternion(a / divider, b / divider, c / divider, d / divider);
    }

    /**
     * Dot product of quaternions. (p*conjugate(q) + q*conjugate(p))/2
     *
     * @param q factor
     * @return dot product of this and q
     */
    public Quaternion dotMult(Quaternion q) {
        return this.times(q.conjugate()).plus(q.times(this.conjugate())).divide(2);
    }

    /**
     * Integer hashCode has to be the same for equal objects.
     *
     * @return hashcode
     */
    @Override
    public int hashCode() {
        return (int) (a * 11110 + b * 22221 + c * 33332 + d * 44443);
    }

    /**
     * Norm of the quaternion. Expressed by the formula
     * norm(a+bi+cj+dk) = Math.sqrt(a*a+b*b+c*c+d*d)
     *
     * @return norm of <code>this</code> (norm is a real number)
     */
    public double norm() {
        return Math.sqrt(a * a + b * b + c * c + d * d);
    }

    /**
     * Main method for testing purposes.
     *
     * @param arg command line parameters
     */
    public void main(String[] arg) {
        Quaternion arv1 = new Quaternion(-1., 1, 2., -2.);
        if (arg.length > 0)
            arv1 = valueOf(arg[0]);
        System.out.println("first: " + arv1.toString());
        System.out.println("real: " + arv1.getRpart());
        System.out.println("imagi: " + arv1.getIpart());
        System.out.println("imagj: " + arv1.getJpart());
        System.out.println("imagk: " + arv1.getKpart());
        System.out.println("isZero: " + arv1.isZero());
        System.out.println("conjugate: " + arv1.conjugate());
        System.out.println("opposite: " + arv1.opposite());
        System.out.println("hashCode: " + arv1.hashCode());
        Quaternion res = null;
        try {
            res = (Quaternion) arv1.clone();
        } catch (CloneNotSupportedException e) {
        }
        ;
        System.out.println("clone equals to original: " + res.equals(arv1));
        System.out.println("clone is not the same object: " + (res != arv1));
        System.out.println("hashCode: " + res.hashCode());
        res = valueOf(arv1.toString());
        System.out.println("string conversion equals to original: "
                + res.equals(arv1));
        Quaternion arv2 = new Quaternion(1., -2., -1., 2.);
        if (arg.length > 1)
            arv2 = valueOf(arg[1]);
        System.out.println("second: " + arv2.toString());
        System.out.println("hashCode: " + arv2.hashCode());
        System.out.println("equals: " + arv1.equals(arv2));
        res = arv1.plus(arv2);
        System.out.println("plus: " + res);
        System.out.println("times: " + arv1.times(arv2));
        System.out.println("minus: " + arv1.minus(arv2));
        double mm = arv1.norm();
        System.out.println("norm: " + mm);
        System.out.println("inverse: " + arv1.inverse());
        System.out.println("divideByRight: " + arv1.divideByRight(arv2));
        System.out.println("divideByLeft: " + arv1.divideByLeft(arv2));
        System.out.println("dotMult: " + arv1.dotMult(arv2));
    }
}
// end of file
